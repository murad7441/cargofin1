package myapp.repo.impl;

import myapp.model.Airport;
import myapp.model.Country;
import myapp.repo.interfaces.AirportRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AirportRepoImpl implements AirportRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void save(Airport airport) {
        String saveSQL="INSERT INTO lb_airports (airport_name,IATA_code,ICAO_code, ref_city,time_zone,country_id) VALUES (?,?,?,?,?,?)";
        jdbcTemplate.update(saveSQL,airport.getName(),airport.getIATAcode(),airport.getICAOcode(),airport.getRefCity(),airport.getRefCity(),airport.getCountry().getId());
    }

    @Override
    public void update(Airport airport) {
        String updateSQL="UPDATE lb_airports SET airport_name=?,IATA_code=?,ICAO_code=?, ref_city=?,time_zone=?,country_id=? WHERE id=?";
        jdbcTemplate.update(updateSQL,airport.getName(),airport.getIATAcode(),airport.getICAOcode(),airport.getRefCity(),airport.getTimeZone(),airport.getCountry().getId(),airport.getId());
    }

    @Override
    public List<Airport> getDictionary() {
        String selectSQL="Select id as id,airport_name as airportName from lb_airports";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(Airport.class));
    }

    @Override
    public List<Airport> get() {
    String selectSQL = "Select lb_airports.id as id,airport_name as name,IATA_code as IATAcode,ICAO_code as ICAOcode, ref_city as refCity,time_zone as timeZone,country.cnt_name as countryName from lb_airports LEFT JOIN lb_countries country on lb_airports.country_id = country.id";

        List<Airport>airports=new ArrayList<>();
       jdbcTemplate.queryForList(selectSQL).forEach(a->{
           Airport airport=new Airport();
           airport.setId((Integer) a.get("id"));
           airport.setName((String) a.get("name"));
           airport.setIATAcode((String) a.get("IATAcode"));
           airport.setICAOcode((String) a.get("ICAOcode"));
           airport.setRefCity((String) a.get("refCity"));
           airport.setTimeZone((Integer) a.get("timeZone"));

           Country country=new Country();
           country.setName((String) a.get("countryName"));

           airport.setCountry(country);

           airports.add(airport);
       });



        return airports;

    }
}
