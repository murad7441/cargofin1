package myapp.repo.impl;

import myapp.model.AirplanePosition;
import myapp.model.Airport;
import myapp.repo.interfaces.AirplanePositionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AirplanePositionRepoImpl implements AirplanePositionRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public void save(AirplanePosition airplanePosition) {
        //TODO fleet id temasin duzelt staticdi o
        String saveSQL="INSERT  into lb_airplane_positions (position_code, position_order, BA, IsBulkPosition, MaxWeight,fleet_id) VALUES (?,?,?,?,?,?)";
        jdbcTemplate.update(saveSQL,airplanePosition.getPositionCode(),airplanePosition.getPositionOrder(),airplanePosition.getBA(),airplanePosition.isBulkPosition(),airplanePosition.getMaxWeight(),3);
    }

    @Override
    public void update(AirplanePosition airplanePosition) {
        String updateSQL="UPDATE lb_airplane_positions SET position_code=?, position_order=?, BA=?, IsBulkPosition=?, MaxWeight=? WHERE id=?";
        jdbcTemplate.update(updateSQL,airplanePosition.getPositionCode(),airplanePosition.getPositionOrder(),airplanePosition.getBA(),airplanePosition.isBulkPosition(),airplanePosition.getMaxWeight(),airplanePosition.getId());
    }

    @Override
    public List<AirplanePosition> getDictionary() {
        String selectSQL="SELECT id as id ,position_code as positionCode from lb_airplane_positions";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(AirplanePosition.class));
    }

    @Override
    public List<AirplanePosition> get() {
        String selectSQL="SELECT position_code as positionCode, position_order as positionOrder, BA as BA, IsBulkPosition as bulkPosition, MaxWeight as MaxWeight from lb_airplane_positions";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(AirplanePosition.class));
    }
}
