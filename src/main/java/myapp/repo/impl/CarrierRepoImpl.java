package myapp.repo.impl;

import myapp.model.Carrier;
import myapp.repo.interfaces.CarrierRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarrierRepoImpl implements CarrierRepo {

  @Autowired JdbcTemplate jdbcTemplate;

  @Override
  public void save(Carrier carrier) {
    String saveSQL = "INSERT INTO lb_carriers(business_name, icao, iata_2, iata_3, address,is_base) VALUES (?,?,?,?,?,?)";
    jdbcTemplate.update(
        saveSQL,
        carrier.getBusinessName(),
        carrier.getICAO(),
        carrier.getIata2(),
        carrier.getIata3(),
        carrier.getAddress(),
        carrier.getIsBase());
  }

  @Override
  public void update(Carrier carrier) {
    String updateSQL = "Update lb_carriers SET business_name=?, icao=?, iata_2=?, iata_3=?, address=?,is_base=? WHERE id=?";
    jdbcTemplate.update(
        updateSQL,
        carrier.getBusinessName(),
        carrier.getICAO(),
        carrier.getIata2(),
        carrier.getIata3(),
        carrier.getAddress(),
        carrier.getIsBase(),
        carrier.getId());
  }

  @Override
  public List<Carrier> getDictionary() {
    String selectSQL = "Select id as id,business_name as businessName from lb_carriers";
      return jdbcTemplate.query(selectSQL, new BeanPropertyRowMapper(Carrier.class));
  }

  @Override
  public List<Carrier> get() {
    String selectSQL = "Select id as id,business_name as businessName, icao as ICAO, iata_2 as iata2, iata_3 as iata3, address as address,is_base as isBase from lb_carriers";
    return jdbcTemplate.query(selectSQL, new BeanPropertyRowMapper(Carrier.class));
  }
}
