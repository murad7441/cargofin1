package myapp.repo.impl;

import myapp.model.SubArea;
import myapp.repo.interfaces.SubAreaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SubAreaRepoImpl implements SubAreaRepo {


    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public void save(SubArea subArea) {
        String saveSQl="INSERT  INTO lb_sub_area (iata_sub_area_name, iata_area_code) VALUES (?,?)";

        jdbcTemplate.update(saveSQl,subArea.getName(),subArea.getIataAreaCode());
    }

    @Override
    public void update(SubArea subArea) {
        String updateSQL="UPDATE lb_sub_area SET iata_sub_area_name=?,iata_area_code=? WHERE  id=?";
        jdbcTemplate.update(updateSQL,subArea.getName(),subArea.getIataAreaCode(),subArea.getId());
    }

    @Override
    public List<SubArea> getDictionary() {
        String selectSQL="SELECT  id as id ,iata_sub_area_name as name,iata_area_code as iataAreaCode from lb_sub_area";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(SubArea.class));
    }

    @Override
    public List<SubArea> get() {
        String selectSQL="SELECT id as id, iata_sub_area_name as name,iata_area_code as iataAreaCode from lb_sub_area";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(SubArea.class));
        }
}
