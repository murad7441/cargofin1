package myapp.repo.interfaces;

import myapp.model.Carrier;
import myapp.model.SubArea;

import java.util.List;

public interface CarrierRepo {
    void save(Carrier carrier);
    void update(Carrier carrier);
    List<Carrier> getDictionary();
    List<Carrier>get();
}
