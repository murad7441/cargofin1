package myapp.repo.interfaces;

import myapp.model.AirplanePosition;
import myapp.model.Airport;

import java.util.List;

public interface AirplanePositionRepo {
    void save(AirplanePosition airplanePosition);
    void update(AirplanePosition airplanePosition);
    List<AirplanePosition>getDictionary();
    List<AirplanePosition>get();
}
