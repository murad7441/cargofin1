package myapp.repo.interfaces;

import myapp.model.Airplane;
import myapp.model.Airport;

import java.util.List;

public interface AirplaneRepo {

    void save(Airplane airplane);
    void update(Airplane airplane);
    List<Airplane> getDictionary();
    List<Airplane>get();
}
