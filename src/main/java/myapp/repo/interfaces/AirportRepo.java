package myapp.repo.interfaces;

import myapp.model.Airport;
import myapp.model.Country;

import java.util.List;

public interface AirportRepo {
    void save(Airport airport);
    void update(Airport airport);
    List<Airport>getDictionary();
    List<Airport>get();
}
