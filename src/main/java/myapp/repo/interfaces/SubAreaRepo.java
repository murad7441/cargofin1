package myapp.repo.interfaces;

import myapp.model.SubArea;

import java.util.List;

public interface SubAreaRepo {
    void save(SubArea subArea);
    void update(SubArea subArea);
    List<SubArea>getDictionary();
    List<SubArea>get();
}
