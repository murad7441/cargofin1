package myapp.service.interfaces;

import myapp.model.Airplane;

import java.util.List;

public interface AirplaneService {
    void save(Airplane airplane);
    void update(Airplane airplane);
    List<Airplane> getDictionary();
    List<Airplane>get();
}
