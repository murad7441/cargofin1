package myapp.service.impl;

import myapp.model.Airport;
import myapp.model.Country;
import myapp.repo.interfaces.AirportRepo;
import myapp.service.interfaces.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    @Autowired
    AirportRepo airportRepo;


    @Override
    public void save(Airport airport) {
        airportRepo.save(airport);
    }

    @Override
    public void update(Airport airport) {
        airportRepo.update(airport);
    }

    @Override
    public List<Airport> getDictionary() {
        return airportRepo.getDictionary();
    }

    @Override
    public List<Airport> get() {
        return airportRepo.get();
    }
}
