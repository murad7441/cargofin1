package myapp.service.impl;

import myapp.model.AirplanePosition;
import myapp.repo.interfaces.AirplanePositionRepo;
import myapp.service.interfaces.AirplanePositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplanePositionServiceImpl implements AirplanePositionService {

   @Autowired
   AirplanePositionRepo airplanePositionRepo;

    @Override
    public void save(AirplanePosition airplanePosition) {
        airplanePositionRepo.save(airplanePosition);
    }

    @Override
    public void update(AirplanePosition airplanePosition) {
        airplanePositionRepo.update(airplanePosition);
    }

    @Override
    public List<AirplanePosition> getDictionary() {
        return airplanePositionRepo.getDictionary();
    }

    @Override
    public List<AirplanePosition> get() {
        return airplanePositionRepo.get();
    }
}
