package myapp.service.impl;

import myapp.model.SubArea;
import myapp.repo.interfaces.SubAreaRepo;
import myapp.service.interfaces.SubAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubAreaServiceImpl implements SubAreaService {

    @Autowired
    SubAreaRepo subAreaRepo;


    @Override
    public void save(SubArea subArea) {
        subAreaRepo.save(subArea);
    }

    @Override
    public void update(SubArea subArea) {
        subAreaRepo.update(subArea);
    }

    @Override
    public List<SubArea> getDictionary() {
        return subAreaRepo.getDictionary();
    }

    @Override
    public List<SubArea> get() {
        return subAreaRepo.get();
    }

}
