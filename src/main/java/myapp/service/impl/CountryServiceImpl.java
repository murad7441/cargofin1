package myapp.service.impl;

import myapp.model.Country;
import myapp.model.SubArea;
import myapp.repo.interfaces.CountryRepo;
import myapp.repo.interfaces.SubAreaRepo;
import myapp.service.interfaces.CountryService;
import myapp.service.interfaces.SubAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepo countryRepo;


    @Override
    public void save(Country country) {
        countryRepo.save(country);
    }

    @Override
    public void update(Country country) {
        countryRepo.update(country);
    }

    @Override
    public List<Country> getDictionary() {
        return countryRepo.getDictionary();
    }

    @Override
    public List<Country> get() {
        return countryRepo.get();
    }
}
