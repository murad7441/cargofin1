package myapp.controller;

import myapp.model.Airport;
import myapp.model.Country;
import myapp.service.interfaces.AirportService;
import myapp.service.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/airport")
public class AirportController {

    @Autowired
    AirportService airportService;

    @Autowired
    CountryService countryService;


    @GetMapping
    public String get(Map<String, Object> model){
        model.put("airports",airportService.get());
        model.put("countries",countryService.getDictionary());
        return "airports";
    }
   @ResponseBody
    @PostMapping("/add")
    public String add(Airport airport,Integer country_id){
        Country country=new Country();
        country.setId(country_id);
        airport.setCountry(country);

        airportService.save(airport);

        return "ok";
    }
    @ResponseBody
    @PostMapping("/edit")
    public String edit(Airport airport,Integer country_id){
        Country country=new Country();
        country.setId(country_id);
        airport.setCountry(country);

        airportService.update(airport);

        return "ok";
    }
}
