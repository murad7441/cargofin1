package myapp.controller;

import myapp.model.SubArea;
import myapp.service.interfaces.SubAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/subArea")
public class SubAreaController {

    @Autowired
    SubAreaService subAreaService;

    @GetMapping
    public String test(Map<String, Object> model){

        model.put("subAreaList",subAreaService.get());

        return "sub_area";

    }

    @ResponseBody
    @PostMapping("/add")
    public String add(SubArea subArea){

        subAreaService.save(subArea);

        return "ok";

    }

    @ResponseBody
    @PostMapping("/update")
    public String update(SubArea subArea){
                System.out.println(subArea.getId());

        System.out.println(subArea.getId());
        System.out.println(subArea.getIataAreaCode());

        subAreaService.update(subArea);

        return "ok";

    }

}
