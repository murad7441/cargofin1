package myapp;


import com.zaxxer.hikari.HikariPoolMXBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.sql.DataSource;
import java.lang.management.ManagementFactory;

@org.springframework.boot.autoconfigure.SpringBootApplication
public class SpringBootApplication extends SpringBootServletInitializer {

    @Autowired
    DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public static void main(String[] args) {



        SpringApplication.run(SpringBootApplication.class, args);
    }

   /* @Override
    public void run(String... args) {
      *//*  List<Integer> result = jdbcTemplate.query(
                "SELECT cat_id FROM CargoFinSW.dbo.airplanes",
                (rs, rowNum) -> new Integer(rs.getInt("cat_id"))
        );
        result.forEach(r->{
            System.out.println(r);
        });
        System.out.println("DATASOURCE = " + dataSource);*//*

        /// Get dbcp2 datasource settings
        // BasicDataSource newds = (BasicDataSource) dataSource;
        // System.out.println("BasicDataSource = " + newds.getInitialSize());



        System.out.println("Done!");

        exit(0);
    }*/



    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(SpringBootApplication.class);
    }
}
