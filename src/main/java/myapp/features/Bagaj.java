package myapp.features;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Bagaj {

    private static final String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String DB_CONNECTION = "jdbc:sqlserver://192.168.1.20:1433;database=PAX_Transit";
    private static final String DB_USER = "CargoFin";
    private static final String DB_PASSWORD = "CargFin2018@";



    public static List<String> transit(String date) throws SQLException {


        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        List<String> resultList = null;
    String selectSQL =
        " EXEC [dbo].[GET_transit_pax_for_android] @Date = N'" + date + "'";

        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);


            // execute select SQL stetement
            ResultSet rs = preparedStatement.executeQuery();
            resultList=new ArrayList<String>();
            while (rs.next()) {
                resultList.add(rs.getString(1)+ "||"+rs.getString(2)+ "||"+rs.getString(3)+ "||"+rs.getString(4));

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return resultList;
    }


    private static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(DB_DRIVER);

        } catch (ClassNotFoundException e) {

            System.out.println(e.getMessage());

        }

        try {

            dbConnection = DriverManager.getConnection(
                    DB_CONNECTION, DB_USER,DB_PASSWORD);
            return dbConnection;

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return dbConnection;

    }
}
