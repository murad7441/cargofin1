package myapp.features;

import java.util.List;

public class Utils {


    public static boolean isNull(String text){
        if(text==null || text.equals("") || text.equals("null"))return true;
        return false;
    }


    public static boolean isNull(int n){
        if (n==0)return true;
        return false;
    }

    public static boolean isNull(Object n){
        if (n==null)return true;
        return false;
    }

    public static boolean isNull(List<?> list) {
        if (list == null || list.size() == 0) return true;
        return false;
    }


    public static boolean isSingle(List<?> list) {
        if (list == null || list.size()!=1) return false;
        return true;
    }
}
