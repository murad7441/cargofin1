package myapp.model;

public class AirplanePosition {
    private long id;
    private String positionCode;
    private int positionOrder;
    private float BA;
    private boolean bulkPosition;
    private float MaxWeight;
    private String editedBy;
    private String editedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public int getPositionOrder() {
        return positionOrder;
    }

    public void setPositionOrder(int positionOrder) {
        this.positionOrder = positionOrder;
    }

    public float getBA() {
        return BA;
    }

    public void setBA(float BA) {
        this.BA = BA;
    }

    public boolean isBulkPosition() {
        return bulkPosition;
    }

    public void setBulkPosition(boolean bulkPosition) {
        this.bulkPosition = bulkPosition;
    }

    public float getMaxWeight() {
        return MaxWeight;
    }

    public void setMaxWeight(float maxWeight) {
        MaxWeight = maxWeight;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public String getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(String editedDate) {
        this.editedDate = editedDate;
    }
}
