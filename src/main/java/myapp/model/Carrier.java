package myapp.model;

import java.time.LocalDateTime;

public class Carrier {

  private long id;
  private String businessName;
  private Country country;
  private Integer isBase;
  private String ICAO;
  private String iata2;
  private String iata3;
  private String editedBy;
  private String address;
  private LocalDateTime editedDate;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getBusinessName() {
    return businessName;
  }

  public void setBusinessName(String businessName) {
    this.businessName = businessName;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public Integer getIsBase() {
    return isBase;
  }

  public void setIsBase(Integer isBase) {
    this.isBase = isBase;
  }

  public String getICAO() {
    return ICAO;
  }

  public void setICAO(String ICAO) {
    this.ICAO = ICAO;
  }

  public String getIata2() {
    return iata2;
  }

  public void setIata2(String iata2) {
    this.iata2 = iata2;
  }

  public String getIata3() {
    return iata3;
  }

  public void setIata3(String iata3) {
    this.iata3 = iata3;
  }

  public String getEditedBy() {
    return editedBy;
  }

  public void setEditedBy(String editedBy) {
    this.editedBy = editedBy;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public LocalDateTime getEditedDate() {
    return editedDate;
  }

  public void setEditedDate(LocalDateTime editedDate) {
    this.editedDate = editedDate;
  }
}
