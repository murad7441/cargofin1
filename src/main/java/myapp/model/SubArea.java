package myapp.model;

import java.time.LocalDateTime;

public class SubArea {
    private Integer id;
    private String name;
    private Short iataAreaCode;
    private String editedBy;
    private LocalDateTime editedDate;

    public SubArea() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getIataAreaCode() {
        return iataAreaCode;
    }

    public void setIataAreaCode(Short iataAreaCode) {
        this.iataAreaCode = iataAreaCode;
    }
    public String getEditedBy() {
        return editedBy;
    }
    public LocalDateTime getEditedDate() {
        return editedDate;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public void setEditedDate(LocalDateTime editedDate) {
        this.editedDate = editedDate;
    }
}
