package myapp.webservice;

import com.zaxxer.hikari.HikariPoolMXBean;
import myapp.features.Bagaj;
import myapp.features.Camares;
import myapp.features.Passport;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @GetMapping(value = "/active")
    public ResponseEntity active(){
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName poolName = null;
        int a=100000;
        try {
            poolName = new ObjectName("com.zaxxer.hikari:type=Pool (myPool)");


            HikariPoolMXBean poolProxy = JMX.newMXBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
             a=(Integer) mBeanServer.getAttribute(poolName, "ActiveConnections");

        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        } catch (AttributeNotFoundException e) {
            e.printStackTrace();
        } catch (InstanceNotFoundException e) {
            e.printStackTrace();
        } catch (ReflectionException e) {
            e.printStackTrace();
        } catch (MBeanException e) {
            e.printStackTrace();
        }


        return ResponseEntity.ok(String.valueOf(a));
    }


    @PostMapping(value = "/barcode",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity ping(String from, String to){
        List<String>result=null;
        try {
            result=Camares.selectRecordsByBarcode(from,to);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(e.getMessage());

        }
        return ResponseEntity.ok(result);
    }


    @PostMapping(value = "/manual",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity manual(String from, String to){
        List<String>result=null;
        try {
            result=Camares.selectRecordsByManual(from,to);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(e.getMessage());

        }
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "/transit",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity transit(String date){
        List<String>result=null;
        try {
            result=Bagaj.transit(date);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(e.getMessage());

        }
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "/passport",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity passport(String date){
        List<String>result=null;
        try {
            result=Passport.passport(date);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(e.getMessage());

        }
        return ResponseEntity.ok(result);
    }

}
