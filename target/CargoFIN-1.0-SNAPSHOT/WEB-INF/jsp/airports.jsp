<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/favicon.ico" type="image/ico"/>

    <title>Admin CargoFIN| Silk Way LLC</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="index.html" class="site_title"><i class="plane fa fa-plane"></i> <span
                    class="cargo">CARGO FIN</span></a>
        </div>

        <ul class="list-unstyled components navList">
            <li>
                <a href="#"><i class="fa fa-home"></i> Dashboard</a>
                <a href="#"><i class="fa fa-television"></i> Stars Components</a>
                <a href="#"><i class="fa fa-list"></i> Forms</a>
                <a href="#"><i class="fa fa-file-o"></i> Pages</a>
                <a href="#"><i class="fa fa-bar-chart"></i> Graphs and Statistics</a>
                <a href="#"><i class="fa fa-quote-right"></i> Quotes</a>
                <a href="#"><i class="fa fa-table"></i> Table</a>
                <a href="#"><i class="fa fa-map-marker"></i> Maps</a>
                <a href="#"><i class="fa fa-book"></i> Documentation</a>

                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><i class="fa fa-file-text"></i>
                    Dictionaries</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="/subArea">Sub Areas</a></li>
                    <li><a href="/country">Countries</a></li>
                    <li><a href="/airport">Airports</a></li>
                </ul>
            </li>
        </ul>

    </nav>

    <!-- Page Content Holder -->
    <div id="content">

        <div class="nvbar">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="navbar-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                    <div style="float: right;" class="dropdown">
                        <i class="fa fa-user avatar dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-user-circle" aria-hidden="true"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-inbox" aria-hidden="true"></i>Inbox</a></li>
                            <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
                            <li><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i>Help</a></li>
                            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                <p class="table_title">All Airports
                    <button class="btn btn-primary add" data-toggle="modal" data-target="#add-modal"><span><i
                            class="fa fa-plus"></i>Add Airport</span></button>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-9 col-lg-9 col-md-9 col-xs-12">
                <div class="frame">
                    <div class="table-responsive">
                        <table class="table table-hover table-responsive text-center">
                            <thead>
                            <tr>
                                <th class="text-center">Airport</br>Name</th>
                                <th class="text-center">IATA</br>Code</th>
                                <th class="text-center">ICAO</br>Code</th>
                                <th class="text-center">Country</th>
                                <th class="text-center">Time</br>Zone</th>
                                <th class="text-center">Ref</br>City</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${airports}" var="airport">

                                <tr>
                                    <td>${airport.name}</td>
                                    <td>${airport.IATAcode}</td>
                                    <td>${airport.ICAOcode}</td>
                                    <td>${airport.country.name}</td>
                                    <td>${airport.timeZone}</td>
                                    <td>${airport.refCity}</td>
                                    <td>
                                        <i class="fa fa-pencil-square-o edit" aria-hidden="true" data-toggle="modal"
                                           data-target="#edit-modal" data-id="${airport.id}" data-name="${airport.name}" data-iata="${airport.IATAcode}"
                                           data-icao="${airport.ICAOcode}" data-country="${airport.country.name}" data-time="${airport.timeZone}" data-city="${airport.refCity}"></i>
                                        <i class="fa fa-trash-o delete" aria-hidden="true" data-toggle="modal"
                                           data-target="#delete-modal"></i>
                                    </td>
                                </tr>
                            </c:forEach>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- start add modal -->
<div id="add-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Add Airport</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label mb-10">Airport Name:</label>
                        <input type="text" id="nameAdd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">IATA Code:</label>
                        <input type="text" id="IATAcodeAdd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label mb-10">ICAO Code:</label>
                        <input type="text" id="ICAOcodeAdd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10 text-left">Country:</label>
                        <select class="form-control" id="CountryAdd">
                            <option value="0">Choose Country</option>
                            <c:forEach items="${countries}" var="country">
                                <option value="${country.id}">${country.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Time Zone:</label>
                        <input type="text" id="timeZoneAdd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Ref City:</label>
                        <input type="text" id="refCityAdd" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="add-airport" class="btn btn-primary modalButton">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end add modal -->
<!-- start edit modal -->
<div id="edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Edit Airport</h5>
            </div>
            <div class="modal-body">
                <form>
                    <input id="idUpdate" type="hidden" class="form-control">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label mb-10">Airport Name:</label>
                        <input id="nameUpdate" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">IATA Code:</label>
                        <input id="IATAcodeUpdate" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label mb-10">ICAO Code:</label>
                        <input id="ICAOcodeUpdate" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10 text-left">Country:</label>
                        <select  id="CountryUpdate" class="form-control">
                            <option value="0">Choose Country</option>
                            <c:forEach items="${countries}" var="country">
                                <option value="${country.id}">${country.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Time Zone:</label>
                        <input id="timeZoneUpdate" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Ref City:</label>
                        <input id="refCityUpdate" type="text" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="edit-airport" class="btn btn-primary modalButton">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end edit modal -->
<!-- start delete modal -->
<div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Delete Airport</h5>
            </div>
            <div class="modal-body">
                <p>Are You Sure to Delete This Airport?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary modalButton">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end delete modal -->


<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $(this).toggleClass('active');
        });



        $('#add-airport').click(function () {
            var name = $("#nameAdd").val();
            var IATAcode = $("#IATAcodeAdd").val();
            var ICAOcode = $("#ICAOcodeAdd").val();
            var country_id = $("#CountryAdd").val();
            var timeZone = $("#timeZoneAdd").val();
            var refCity = $("#refCityAdd").val();

            if (country_id == 0) {
                country_id = null;
            }


            $.ajax({
                type: "POST",
                url: "/airport/add",
                data: {
                    name: name,
                    IATAcode: IATAcode,
                    ICAOcode: ICAOcode,
                    country_id: country_id,
                    timeZone: timeZone,
                    refCity: refCity
                },
                success: function (data) {
                    $('#add-modal').modal('hide');
                    location.reload();

                },
                error: function () {
                    alert("error");
                }
            });

        });




        $('#edit-airport').click(function () {
            var name = $("#nameUpdate").val();
            var IATAcode = $("#IATAcodeUpdate").val();
            var ICAOcode = $("#ICAOcodeUpdate").val();
            var country_id = $("#CountryUpdate").val();
            var timeZone = $("#timeZoneUpdate").val();
            var refCity = $("#refCityUpdate").val();
            var id=$("#idUpdate").val();

            if (country_id == 0) {
                country_id = null;
            }


            $.ajax({
                type: "POST",
                url: "/airport/edit",
                data: {
                    id:id,
                    name: name,
                    IATAcode: IATAcode,
                    ICAOcode: ICAOcode,
                    country_id: country_id,
                    timeZone: timeZone,
                    refCity: refCity
                },
                success: function (data) {
                    $('#edit-modal').modal('hide');
                    location.reload();

                },
                error: function () {
                    alert("error");
                }
            });

        });







    });
</script>
<script>
    $('#edit-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name = button.data('name')
        var iata = button.data('iata')
        var icao = button.data('icao')
        var country = button.data('country')
        var time = button.data('time')
        var city = button.data('city')

        var modal = $(this)
        modal.find('#nameUpdate').val(name)
        modal.find('#IATAcodeUpdate').val(iata)
        modal.find('#ICAOcodeUpdate').val(icao)
        modal.find('#timeZoneUpdate').val(time)
        modal.find('#refCityUpdate').val(city)
        modal.find('#idUpdate').val(id)

        $("#CountryUpdate option").filter(function () {
            return this.text == country;
        }).attr('selected', true);



    })
</script>

</body>
</html>
