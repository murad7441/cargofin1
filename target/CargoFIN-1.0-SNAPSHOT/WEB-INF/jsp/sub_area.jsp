<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/favicon.ico" type="image/ico"/>

    <title>Admin CargoFIN| Silk Way LLC</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="index.html" class="site_title"><i class="plane fa fa-plane"></i> <span
                    class="cargo">CARGO FIN</span></a>
        </div>

        <ul class="list-unstyled components navList">
            <li>
                <a href="#"><i class="fa fa-home"></i> Dashboard</a>
                <a href="#"><i class="fa fa-television"></i> Stars Components</a>
                <a href="#"><i class="fa fa-list"></i> Forms</a>
                <a href="#"><i class="fa fa-file-o"></i> Pages</a>
                <a href="#"><i class="fa fa-bar-chart"></i> Graphs and Statistics</a>
                <a href="#"><i class="fa fa-quote-right"></i> Quotes</a>
                <a href="#"><i class="fa fa-table"></i> Table</a>
                <a href="#"><i class="fa fa-map-marker"></i> Maps</a>
                <a href="#"><i class="fa fa-book"></i> Documentation</a>

                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><i class="fa fa-file-text"></i>
                    Dictionaries</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="/subArea">Sub Areas</a></li>
                    <li><a href="/country">Countries</a></li>
                    <li><a href="/airport">Airports</a></li>
                </ul>
            </li>
        </ul>

    </nav>

    <!-- Page Content Holder -->
    <div id="content">

        <div class="nvbar">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="navbar-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                    <div style="float: right;" class="dropdown">
                        <i class="fa fa-user avatar dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-user-circle" aria-hidden="true"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-inbox" aria-hidden="true"></i>Inbox</a></li>
                            <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
                            <li><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i>Help</a></li>
                            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-lg-6 col-md-6 col-xs-12">
                <p class="table_title">All Sub Areas
                    <button class="btn btn-primary add" data-toggle="modal" data-target="#add-modal"><span><i
                            class="fa fa-plus"></i>Add Sub Area</span></button>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5 col-lg-5 col-md-5 col-xs-12">
                <div class="frame">
                    <div class="table-responsive">
                        <table class="table table-hover table-responsive text-center">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">IATA </br>Area</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${subAreaList}" var="subArea">

                                <tr>
                                    <td>${subArea.name}</td>
                                    <td>${subArea.iataAreaCode}</td>
                                    <td>
                                        <i class="fa fa-pencil-square-o edit" aria-hidden="true" data-toggle="modal"
                                           data-target="#edit-modal" data-id="${subArea.id}"
                                           data-name="${subArea.name}" data-iata="${subArea.iataAreaCode}"></i>
                                        <i class="fa fa-trash-o delete" aria-hidden="true" data-toggle="modal"
                                           data-target="#delete-modal"></i>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- start add modal -->
<div id="add-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Add Sub Area</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" id="nameAdd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="iata" class="">IATA Area:</label>
                        <input type="text" id="iataAreaCodeAdd" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary modalButton" id="add-subArea">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end add modal -->
<!-- start edit modal -->
<div id="edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Edit Sub Area</h5>
            </div>
            <div class="modal-body">
                <form>

                    <input type="hidden" id="idUpdate" class="form-control">

                    <div class="form-group">
                        <label for="nameUpdate">Name:</label>
                        <input type="text" id="nameUpdate" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="iataAreaCodeUpdate" class="">IATA Area:</label>
                        <input id="iataAreaCodeUpdate" type="text" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="edit-subArea" class="btn btn-primary modalButton">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end edit modal -->
<!-- start delete modal -->
<div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Delete Sub Area</h5>
            </div>
            <div class="modal-body">
                <p>Are You Sure to Delete This Sub Area?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary modalButton">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- /.end delete modal -->


<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $(this).toggleClass('active');
        });


        $('#add-subArea').click(function () {
            var name = $("#nameAdd").val();
            var iataAreaCode = $("#iataAreaCodeAdd").val();


            $.ajax({
                type: "POST",
                url: "/subArea/add",
                data: {name: name, iataAreaCode: iataAreaCode},
                success: function (data) {
                    $('#add-modal').modal('hide');
                    location.reload();

                },
                error: function () {
                    alert("error");
                }
            });

        });


        $('#edit-subArea').click(function () {
            var nameUpdate = $("#nameUpdate").val();
            var iataAreaCodeUpdate = $("#iataAreaCodeUpdate").val();
            var idUpdate = $("#idUpdate").val();

            $.ajax({
                type: "POST",
                url: "/subArea/update",
                data: {id:idUpdate,name: nameUpdate, iataAreaCode: iataAreaCodeUpdate},
                success: function (data) {
                    $('#edit-modal').modal('hide');
                    // alert("ok")
                    location.reload();

                },
                error: function () {
                    alert("error");
                }
            });

        });


    });
</script>
<script>
    $('#edit-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name = button.data('name')
        var iata = button.data('iata')
        var modal = $(this)
        modal.find('#nameUpdate').val(name)
        modal.find('#iataAreaCodeUpdate').val(iata)
        modal.find('#idUpdate').val(id)

    })
</script>

</body>
</html>
